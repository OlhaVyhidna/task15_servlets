package servletstask.pizzadelivery.model;

public class Order {
  private static int idCounter =1;
  private String userName;
  private String userSurname;
  private String address;
  private String orderedPizzaName;
  private int id;

  public Order() {
  }

  public Order(String userName, String userSurname, String address, String orderedPizzaName) {
    this.userName = userName;
    this.userSurname = userSurname;
    this.address = address;
    this.orderedPizzaName = orderedPizzaName;
    this.id = idCounter;
    idCounter++;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserSurname() {
    return userSurname;
  }

  public void setUserSurname(String userSurname) {
    this.userSurname = userSurname;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getOrderedPizzaName() {
    return orderedPizzaName;
  }

  public void setOrderedPizzaName(String orderedPizzaName) {
    this.orderedPizzaName = orderedPizzaName;
  }

  public int getId() {
    return id;
  }

  @Override
  public String toString() {
    return "Order{" +
        "userName='" + userName + '\'' +
        ", userSurname='" + userSurname + '\'' +
        ", address='" + address + '\'' +
        ", orderedPizzaName='" + orderedPizzaName + '\'' +
        ", id=" + id +
        '}';
  }
}
