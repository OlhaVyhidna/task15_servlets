package servletstask.pizzadelivery.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import servletstask.pizzadelivery.model.Order;

public class OrderServlet extends HttpServlet {

  private static Logger LOG = LogManager.getLogger(OrderServlet.class);
  private static List<Order> orders = new ArrayList<Order>();

  public void init() {
    LOG.info("Servlet " + this.getServletName() + " has started");
  }

  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    LOG.info("Into doGet");
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    out.println("<html><body>");
    out.println("<form action='add-order' method='POST'>" +
        "UserName:<input type='text' name='userName'> " +
        "UserSurname:<input type='text' name='userSurname'> " +
        "Address:<input type='text' name='address'> " +
        "OrderedPizzaName:<input type='text' name='orderedPizzaName'> "
        + "<button type='submit'>Send to server</button></form>");
    out.println("<p> <a href='add-order'>REFRESH</a> </p>");
    out.println("<h2>Orders List</h2>");
    for (Order order : orders) {
      out.println("<p>" + order + "</p>");
    }

    out.println("<form>\n"
        + "  <p><b>DELETE ELEMENT</b></p>\n"
        + "  <p> User id: <input type='text' name='order_id'>\n"
        + "    <input type='button' onclick='remove(this.form.order_id.value)' name='ok' value='Delete User'/>\n"
        + "  </p>\n"
        + "</form>");

    out.println("<script type='text/javascript'>\n"
        + "  function remove(id) { fetch('add-order/' + id, {method: 'DELETE'}); }\n"
        + "</script>");

    out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
    out.println("<p>Method: " + req.getMethod() + "</p>");
    out.println("</body></html>");
  }

  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    String username = req.getParameter("userName");
    String userSurname = req.getParameter("userSurname");
    String address = req.getParameter("address");
    String orderedPizzaName = req.getParameter("orderedPizzaName");
    LOG.info("Into doPost() " + username);
    LOG.info("Into doPost() " + userSurname);
    LOG.info("Into doPost() " + address);
    LOG.info("Into doPost() " + orderedPizzaName);
    LOG.info("delete " + req.getParameter("order_id"));
    orders.add(new Order(username, userSurname, address, orderedPizzaName));
    doGet(req, resp);
  }

  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws  IOException {
    LOG.info("----------doDelete------------");
    LOG.info(req.getParameter("order_id"));
    String id = req.getRequestURI();
    LOG.info("URI=" + id);
    id = id.replace("/servletstask-1.0-SNAPSHOT/add-order", "");
    LOG.info("id=" + id);
    orders.remove(Integer.parseInt(id));
  }

  public void destroy() {
    LOG.info("Servlet " + this.getServletName() + " has stopped");
  }
}
